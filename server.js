const express = require("express");
const bodyParser = require("body-parser");
const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const redisClient = require('./redisClient');
const Worker = require('./worker');

//create worker instance to fetch data every second
const worker = new Worker(redisClient, {
    interval: 1000
});

//init redis connection
redisClient.connect(process.env.REDIS_URL);

//register worker to check range of messages every 1ms
worker.register('job', (message) => {
    //log message to server console
    console.log(JSON.parse(message));
});

app.post('/echoAtTime', async (req, res) => {
    const value = req.body;

    if(!value.time) {
        res.status(500);
        return res.send('please specify time');
    }

    //add sorted set to redis
    await redisClient.zadd(['job', value.time, JSON.stringify(value)]);
    return res.send(value);
});

//listen API server
const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
    console.log(`Server listening on port ${PORT}`);
});
