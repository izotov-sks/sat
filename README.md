# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Send Message at Time ###

* APP to echo message to the server console at given time
* 1.0.0

### How to set up? ###

* run:
```
docker-compose up
```
* be sure that you have console messages:
```
Server listening on port 3000
Redis connected
```
* check that APP is running: [http://localhost:3000](http://localhost:3000)
* or use `npm start` to run manually
### How to use? ###
* send curl query:
```
curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"time":"1421308800000","message":"test"}' \
  http://localhost:3000/echoAtTime
```

### Run tests ###
* worker:
```
node testWorker.js
```
* API:
```
node testAPI.js
```