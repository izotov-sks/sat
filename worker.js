const log4js = require('log4js');
log4js.configure({
  appenders: { worker: { type: 'file', filename: 'worker.log' } },
  categories: { default: { appenders: ['worker'], level: 'debug' } }
});
const logger = log4js.getLogger('worker');

function Worker(client, options) {
  const defaultOptions = {
    interval: 1000
  }

  this.redisClient = client;
  this.options = Object.assign({}, defaultOptions, options);
  this.jobs = {};
}

Worker.prototype.cancel = function(jobName) {
  clearInterval(this.jobs[jobName]);
  delete this.jobs[jobName];
}

Worker.prototype.register = function(jobName, handler, prefetch = 5) {
  const stopPoint = prefetch - 1;

  if (this.jobs[jobName]) {
    return Promise.reject(new Error('Job existed'));
  }

  const interval = setInterval(() => {
    //fetch all ASC ordered messages before NOW if server was down
    this.redisClient.zrange([jobName, 0, -1, 'WITHSCORES'])
      .then((data) => {
        logger.debug(`worker "${jobName}": `, data);
        for (let i = 0; i < data.length; i += 2) {
          const value = data[i];
          const score = parseInt(data[i+1], 0);

          if (score <= Date.now()) {
            //process messages
            handler(value);
            //remove messages
            this.redisClient.zrem([jobName, value]);
          }
        }
      })
      .catch((err) => {
        logger.error(`Error getting for job "${jobName}": `, err);
      })
  }, this.options.interval);

  this.jobs[jobName] = interval;
}

module.exports = Worker;
