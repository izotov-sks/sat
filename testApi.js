const request = require('request');

//sending query to show message in a 5 sec
const timeInA5sec = Date.now()+5000;

request.post('http://localhost:3000/echoAtTime', {
    json: {
        time: timeInA5sec,
        message: "test"
    }
}, (error, res, body) => {
    if (error) {
        console.error(error);
        return;
    }
    console.log(`statusCode: ${res.statusCode}`);
    console.log(body);
});
