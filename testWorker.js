const redisClient = require('./redisClient');
const Worker = require('./worker');

redisClient.connect(process.env.REDIS_URL);
const worker = new Worker(redisClient, {
    interval: 1000
});
worker.register('job', (message) => {
    //log message to server console
    console.log(message);
});

// console.log(Date.parse("Tue May 19 21:19:00 +0000 2020"));
// console.log(Date.now()+20000);

redisClient.zadd(['job', Date.now()+5000, 'message after 5 sec is here'])
    .then((data) => {
    console.log('message should be shown in a 5 sec', data);
})
.catch((err) => {
    console.log('error', err)
});

redisClient.zadd(['job', Date.now()+10000, 'message after 10 sec is here'])
    .then((data) => {
    console.log('message should be shown in a 10 sec', data);
})
.catch((err) => {
    console.log('error', err)
});
